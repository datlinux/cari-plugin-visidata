#!/usr/bin/env bash

set -euo pipefail

PIP_REPO="https://pypi.org/rss/project/visidata/releases.xml"
TOOL_NAME="visidata"
TOOL_TEST="visidata"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

list_pip_tags() {
  curl -s $PIP_REPO | sed -n 's/\s*<title>\([0-9]*\)/\1/p' | grep -v "PyPI recent" |  cut -d "<" -f1
}

list_all_versions() {
  list_pip_tags
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"
    touch "$install_path/bin/visidata"
    echo "Virtual env:: $CARI_VENV_PATH/visidata/$version"
    echo "source $CARI_VENV_PATH/visidata/$version/bin/activate" >> "$install_path/bin/visidata"
    echo "$CARI_VENV_PATH/visidata/$version/bin/vd \"\$@\"" >> "$install_path/bin/visidata"
    chmod a+x "$install_path/bin/visidata"
    
    #rm -rf "$CARI_VENV_PATH/visidata/$version"
    mkdir -p "$CARI_VENV_PATH/visidata/$version"
    if [ ! -d $HOME/tmp ]; then
        mkdir $HOME/tmp
    fi
    export TMPDIR=$HOME/tmp
    python3 -m venv "$CARI_VENV_PATH/visidata/$version"
    source "$CARI_VENV_PATH/visidata/$version/bin/activate"
    if [ -z $CARI_PIP_CACHE ]; then
      CARI_PIP_CACHE="$HOME/.pip-cache"
    fi
    python3 -m pip install --cache-dir="$CARI_PIP_CACHE" wheel
    python3 -m pip install --cache-dir="$CARI_PIP_CACHE" visidata=="$version"
    deactivate
    rm -rf $HOME/tmp

    test -x "$CARI_VENV_PATH/visidata/$version/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
